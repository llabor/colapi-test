var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var URLbase = '/apicol/v1'
var users = require('./users.json');

app.use(bodyParser.json());
app.listen(port);
console.log('APICol escuchando en el puerto ' + port + '...');

// Petición GET
app.get('/apicol/v1/users',
  function (req, res) {
    console.log("GET /apicol/v1/users");
    //  res.send({"msg" : "Hola desde apicol"});
    // res.sendfile('./usuarios.json'); // DEPRECATED
    res.sendFile('users.json', {root: __dirname});
// assuming 'users.json' is in the same directory as this script
 // res.sendFile(__dirname + '/users.json');
});

app.get('/apicol/v1/users/:id',
  function(req, res) {
    console.log("GET /apicol/v1/users/:id");
    console.log(req.params);
    console.log(req.params.id);
    console.log("Headers");
    console.log(req.headers);
    res.send(users[req.params.id - 1]);
  });

  app.post(URLbase + '/users',
    function(req, res) {
      console.log("POST /apicol/v1/users");
      var nuevo = req.body;
      nuevo.id = users.length + 1;
      users.push(nuevo);
      console.log(users);
      res.send({"msg" : "Usuario dado de alta correctamente."});
      //res.send({"msg" : "Respuesta de POST /apicol/v1/users"});
  });
